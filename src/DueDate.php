<?php
namespace calculator;

use calculator\config\Config;
use calculator\exceptions\DueDateException;
use calculator\exceptions\ResolvedTimeException;
use calculator\models\DateTimeModel;
use calculator\models\ResolvedTimeModel;

class DueDate
{
    private $dateTime;
    private $turnaroundTime;

    /**
     * DueDate constructor.
     * @param string $dateTime
     * @param int $turnaroundTime
     */
    public function __construct(string $dateTime, int $turnaroundTime)
    {
        $this->setDateTime($dateTime);
        $this->setTurnaroundTime($turnaroundTime);
    }

    /**
     * set the turnaroundTime
     * @param int $turnaroundTime
     */
    private function setTurnaroundTime(int $turnaroundTime): void
    {
        $this->turnaroundTime = $turnaroundTime;
    }

    /**
     * set the dateTime
     * @param string $dateTime
     */
    private function setDateTime(string $dateTime): void
    {
        try {
            $this->dateTime = new \DateTime($dateTime);

            if(!$this->dateTime)
            {
                throw new DueDateException('Nem megfelelő dátum formátum!');
            }

        } catch (DueDateException $e) {
            echo $e->getMessage();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * get the resolved time
     * @return string
     */
    public function getResolvedTime(): string
    {
        $config = new Config();

        $dueDateModel = new DateTimeModel($this->dateTime, $config);
        $resolvedModel = new ResolvedTimeModel($dueDateModel);

        try {
            if($resolvedModel->validate())
            {
                return $resolvedModel->generateDate($this->turnaroundTime)->format('Y-m-d H:i');
            }
        }  catch (ResolvedTimeException $e)  {
            return $e->getMessage();
        }

        return '';
    }
}