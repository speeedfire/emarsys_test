<?php


namespace calculator\services;


use calculator\config\Config;
use calculator\interfaces\DaysInterface;

class WorkingDaysService implements DaysInterface
{
    /**
     * get the days
     * @return array
     */
    public function getDays(): array
    {
        $config = new Config();
        return $config->getConfig('extraWorkDays');
    }
}