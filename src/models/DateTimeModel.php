<?php
namespace calculator\models;

use calculator\config\Config;
use calculator\exceptions\StartDayException;
use calculator\interfaces\DayTimeInterface;

class DateTimeModel implements DayTimeInterface
{
    private $dateTime;
    private $startDay;
    private $endDay;
    private $config;
    private $reportTime;

    /**
     * DateTimeModel constructor.
     * @param \DateTime $dateTime
     * @param Config $config
     */
    public function __construct(\DateTime $dateTime, Config $config)
    {
        $this->dateTime = $dateTime;
        $this->reportTime = $dateTime;
        $this->config = $config;

        $this->setStartDay($this->config->getConfig('startDay'));
        $this->setEndDay($this->config->getConfig('endDay'));
    }

    /**
     * get the reported time
     * @return \DateTime
     */
    public function getReportTime(): \DateTime
    {
        return $this->reportTime;
    }

    /**
     * get the datetime
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * set the start of the day
     * @param string $start
     */
    public function setStartDay(string $start): void
    {
        $this->startDay = clone $this->dateTime;
        $this->startDay->setTime(substr($start, 0, 2), substr($start, 2, 2));
    }

    /**
     * set end of the day
     * @param string $end
     */
    public function setEndDay(string $end): void
    {
        $this->endDay = clone $this->dateTime;
        $this->endDay->setTime(substr($end, 0, 2), substr($end, 2, 2));
    }

    /**
     * get the start of the day
     * @return \DateTime
     */
    public function getStartDay(): \DateTime
    {
        return $this->startDay;
    }

    /**
     * get the end of the day
     * @return \DateTime
     */
    public function getEndDay(): \DateTime
    {
        return $this->endDay;
    }

    /**
     * it is working day?
     * @return bool
     */
    public function getIsWorkingDay(): bool
    {
        $holidays = $this->config->getConfig('extraHolidays');
        $workDays = $this->config->getConfig('workDays');

        if(in_array($this->dateTime->format('Y-m-d'), $holidays) || !in_array($this->dateTime->format('l'), $workDays))
        {
            return false;
        }

        return true;
    }

    /**
     * it is holiday?
     * @return bool
     */
    public function getIsHoliday(): bool
    {
        $extraWorkDays = $this->config->getConfig('extraWorkDays');
        $holidays = $this->config->getConfig('extraHolidays');
        $workDays = $this->config->getConfig('workDays');

        if((in_array($this->dateTime->format('Y-m-d'), $extraWorkDays) || in_array($this->dateTime->format('l'), $workDays)) && !in_array($this->dateTime->format('Y-m-d'), $holidays) )
        {
            return false;
        }

        return true;
    }
}