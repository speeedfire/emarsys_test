<?php


namespace calculator\tests\services;

use calculator\services\WorkingDaysService;

class WorkingDaysServiceTest
{
    private $model;

    public function setUp()
    {
        $this->model = new WorkingDaysService();
    }

    public function testTypeArray()
    {
        $this->assertEquals(is_array($this->model->getDays()), true);
    }
}