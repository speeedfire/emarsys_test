<?php


namespace calculator\tests\services;


use calculator\services\HolidaysService;

class HolidaysServiceTest extends \PHPUnit\Framework\TestCase
{
    private $model;

    public function setUp()
    {
        $this->model = new HolidaysService();
    }

    public function testTypeArray()
    {
        $this->assertEquals(is_array($this->model->getDays()), true);
    }
}