<?php
namespace calculator\tests\models;

use calculator\config\Config;
use calculator\models\ResolvedTimeModel;
use calculator\models\DateTimeModel;
use Mockery;

class ResolvedTimeModelTest  extends \PHPUnit\Framework\TestCase
{
    private $dateTimeModel;
    private $resolvedTimeModel;

    private $config = [
        'startDay' => '0900',
        'endDay' => '1700',
        'workDays' => [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
        ],
        'extraWorkDays' => [
            '2020-04-14',
            '2020-04-19',
            '2020-05-01',
        ],
        'extraHolidays' => [
            '2020-04-04',
            '2020-04-05',
            '2020-04-06',
            '2020-04-16',
            '2020-04-20',
            '2020-05-05',
        ]
    ];

    public function setUp()
    {
        $config = new Config();
        $config->setConfig($this->config);

        $this->dateTimeModel = new DateTimeModel(new \DateTime('2020-04-01 14:00'), $config);
        $this->resolvedTimeModel = new ResolvedTimeModel($this->dateTimeModel);
    }


    public function testValidate()
    {
        $this->assertEquals($this->resolvedTimeModel->validate(), true);
    }

    public function GenerateDateProvider()
    {
        return [
            [0],
            [1],
            [5],
            [16],
            [24],
            [30],
        ];
    }

    /**
     * @dataProvider GenerateDateProvider
     * @throws \calculator\exceptions\ResolvedTimeException
     */
    public function testGenerateDate($turnaroundTime)
    {
        $this->assertEquals($this->resolvedTimeModel->generateDate($turnaroundTime) instanceof \DateTime, true);
    }
}
