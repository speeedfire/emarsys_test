<?php


namespace calculator\services;


use calculator\config\Config;
use calculator\interfaces\DaysInterface;

class HolidaysService implements DaysInterface
{
    /**
     * get the day
     * @return array
     */
    public function getDays(): array
    {
        $config = new Config();
        return $config->getConfig('extraHolidays');
    }
}