<?php
namespace calculator\models;

use calculator\exceptions\ResolvedTimeException;

class ResolvedTimeModel
{
    private $dateTime;

    /**
     * ResolvedTimeModel constructor.
     * @param DateTimeModel $dateTime
     */
    public function __construct(DateTimeModel $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * it is valid?
     * @return bool
     * @throws ResolvedTimeException
     */
    public function validate(): bool
    {
        if(
            $this->dateTime->getEndDay() < $this->dateTime->getReportTime() ||
            $this->dateTime->getStartDay() > $this->dateTime->getReportTime() ||
            $this->dateTime->getIsHoliday()
            )
        {
            throw new ResolvedTimeException('Csak munkaidőben lehet feladni bejelentést!');
        }

        return true;
    }

    /**
     * generate the resolve date
     * @param int $turnaroundTime
     * @return \DateTime
     * @throws ResolvedTimeException
     */
    public function generateDate(int $turnaroundTime): \DateTime
    {
        $this->checkTurnaroundTime($turnaroundTime);

        $reportHour = $this->dateTime->getDateTime()->format('H');
        $startDayHour = $this->dateTime->getStartDay()->format('H');
        $endDayHour = $this->dateTime->getEndDay()->format('H');

        for($i = 0;;)
        {
            if($turnaroundTime == 0)
            {
                break;
            }

            if($this->dateTime->getIsHoliday() || $this->dateTime->getDateTime()->format('Ymd') == date('Ymd') && date('H') > $endDayHour)
            {
                $this->setIfHolidayTime();
                $i++;
                continue;
            }

            if($turnaroundTime >= $endDayHour - $startDayHour)
            {
                $turnaroundTime = $this->setTurnaroundTime($turnaroundTime, $reportHour, $startDayHour, $endDayHour, $i);

                $this->setTheDayHourForLoop($turnaroundTime);
            } else {
                $this->dateTime->getDateTime()->modify('+' . $turnaroundTime . ' hour');
                $turnaroundTime = 0;
            }

            $i++;
        }

        return $this->dateTime->getDateTime();
    }

    /**
     * set he day and hours for loop
     * @param int $turnaroundTime
     */
    private function setTheDayHourForLoop(int $turnaroundTime): void
    {
        if($turnaroundTime != 0) {
            $this->dateTime->getDateTime()->modify('+1 day');
            $this->dateTime->getDateTime()->setTime($this->dateTime->getStartDay()->format('H'), $this->dateTime->getStartDay()->format('i'));
        } else {
            $this->dateTime->getDateTime()->setTime($this->dateTime->getEndDay()->format('H'), $this->dateTime->getEndDay()->format('i'));
        }
    }

    /**
     * set the turnaroundTime
     * @param int $turnaroundTime
     * @param int $reportHour
     * @param int $startDayHour
     * @param int $endDayHour
     * @param int $i
     * @return int
     */
    private function setTurnaroundTime(int $turnaroundTime, int $reportHour, int $startDayHour, int $endDayHour, int $i = 0): int
    {
        if($i === 0) {
            $turnaroundTime -= $endDayHour - $reportHour;
        } else {
            $turnaroundTime -= $endDayHour - $startDayHour;
        }

        return $turnaroundTime;
    }

    /**
     * set the date if it holiday, or end of day
     */
    private function setIfHolidayTime()
    {
        $this->dateTime->getDateTime()->modify('+1 day');
        $this->dateTime->getDateTime()->setTime($this->dateTime->getStartDay()->format('H'), $this->dateTime->getStartDay()->format('i'));
    }

    /**
     * chech the turnaroundTime
     * @param int $turnaroundTime
     * @return string
     * @throws ResolvedTimeException
     */
    private function checkTurnaroundTime(int $turnaroundTime)
    {
        if($turnaroundTime === 0)
        {
            return $this->dateTime->getDateTime()->format('Y-m-d H:i');
        }

        if($turnaroundTime < 0)
        {
            throw new ResolvedTimeException('Az átfutási időnek nagyobbnak kell lennie, mint 0');
        }
    }
}