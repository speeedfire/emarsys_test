<?php
namespace calculator\tests\models;

use calculator\config\Config;
use calculator\models\DateTimeModel;
use Mockery;

class DateTimeModelTest  extends \PHPUnit\Framework\TestCase
{
    private $model;

    private $config = [
        'startDay' => '0900',
        'endDay' => '1700',
        'workDays' => [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
        ],
        'extraWorkDays' => [
            '2020-04-14',
            '2020-04-19',
            '2020-05-01',
        ],
        'extraHolidays' => [
            '2020-04-04',
            '2020-04-05',
            '2020-04-06',
            '2020-04-16',
            '2020-04-20',
            '2020-05-05',
        ]
    ];

    public function setUp()
    {
        $this->model = new DateTimeModel(new \DateTime('2020-04-14'), new Config());
    }

    public function testGetReportTime()
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->shouldReceive('getConfig')
            ->andReturn($this->config);

        $this->assertEquals($this->model->getReportTime() instanceof \DateTime, true);
    }

    public function testGetDateTime()
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->shouldReceive('getConfig')
            ->andReturn($this->config);

        $this->assertEquals($this->model->getDateTime() instanceof \DateTime, true);
    }

    public function testGetStartDay()
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->shouldReceive('getConfig')
            ->andReturn($this->config);

        $this->assertEquals($this->model->getStartDay() instanceof \DateTime, true);
    }

    public function testGetEndDay()
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->shouldReceive('getConfig')
            ->andReturn($this->config);

        $this->assertEquals($this->model->getEndDay() instanceof \DateTime, true);
    }

    public function testGetIsWorkingDay()
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->shouldReceive('getConfig')
            ->andReturn($this->config);

        $this->assertEquals($this->model->getIsWorkingDay(), true);
    }

    public function testGetIsHoliday()
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->shouldReceive('getConfig')
            ->andReturn($this->config);

        $this->assertEquals($this->model->getIsHoliday(), false);
    }
}