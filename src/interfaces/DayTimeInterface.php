<?php
namespace calculator\interfaces;

interface DayTimeInterface
{
    public function getStartDay();
    public function getEndDay();

    public function getIsWorkingDay();
    public function getIsHoliday();
}