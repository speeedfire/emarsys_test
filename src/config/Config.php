<?php
namespace calculator\config;

class Config
{
    private $config = [
        'startDay' => '0900',
        'endDay' => '1700',
        'workDays' => [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
        ],
        'extraWorkDays' => [
            '2020-04-14',
            '2020-04-19',
            '2020-05-01',
        ],
        'extraHolidays' => [
            '2020-04-04',
            '2020-04-05',
            '2020-04-06',
            '2020-04-16',
            '2020-04-20',
            '2020-05-05',
        ]
    ];

    /**
     * set the config
     * @param array $config
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    /**
     * get the config
     * @param string|null $key
     * @return array|mixed
     */
    public function getConfig(string $key = null)
    {
        if ($key === null) {
            return $this->config;
        } elseif (isset($this->config[$key])) {
            return $this->config[$key];
        } else {
            return [];
        }
    }
}