<?php
namespace calculator\tests;

use calculator\config\Config;
use calculator\DueDate;
use Mockery;

class DueDateTest extends \PHPUnit\Framework\TestCase
{
    private $model;

    private $config = [
        'startDay' => '0900',
        'endDay' => '1700',
        'workDays' => [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
        ],
        'extraWorkDays' => [
            '2020-04-14',
            '2020-04-19',
            '2020-05-01',
        ],
        'extraHolidays' => [
            '2020-04-04',
            '2020-04-05',
            '2020-04-06',
            '2020-04-16',
            '2020-04-20',
            '2020-05-05',
        ]
    ];

    public function GenerateDateProvider()
    {
        return [
            ['2020-04-01 14:00', 1],
            ['2020-04-01 14:00', 5],
            ['2020-04-01 14:00', 10],
            ['2020-04-03 14:00', 16],
            ['2020-04-06 14:00', 16],
        ];
    }

    /**
     * @dataProvider GenerateDateProvider
     * @param $dateTime
     * @param $turnaroundTime
     */
    public function testGetResolvedTime($dateTime, $turnaroundTime)
    {
        $configMock = Mockery::mock(Config::class);
        $configMock->shouldReceive('getConfig')
            ->andReturn($this->config);

        $this->model = new DueDate($dateTime, $turnaroundTime);

        $this->assertEquals(is_string($this->model->getResolvedTime()), true);
    }

}