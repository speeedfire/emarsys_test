<?php
namespace calculator\tests\config;

use calculator\config\Config;

class ConfigTest extends \PHPUnit\Framework\TestCase
{
    private $config;

    public function setUp()
    {
        $this->config = new Config();
    }

    public function testTypeArray()
    {
        $this->assertEquals(is_array($this->config->getConfig(null)), true);
    }
}